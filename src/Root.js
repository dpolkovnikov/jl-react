import React from 'react';
import PropTypes from 'prop-types'
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { Login, Product } from './screens';
import { Layout } from './components';

export const Root = ({ store }) => (
  <Layout>
    <Provider store={ store }>
      <Router>
        <Switch>
          <Route path='/login' component={ Login } />
          <Route path='/product/:category?' component={ Product } />
          <Redirect from='/' to='/login' />
        </Switch>
      </Router>
    </Provider>
  </Layout>
);

Root.propTypes = {
  store: PropTypes.object.isRequired
};
