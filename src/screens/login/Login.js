import React, { PureComponent } from 'react';

export class Login extends PureComponent {

  onSubmitHandler = event => {
    event.preventDefault();
    this.props.history.push('/product');
  }

  render() {
    return (
      <div className="row justify-content-md-center">
        <div className="col-md-auto">
          <form onSubmit={ this.onSubmitHandler }>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
              <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Password</label>
              <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    )
  }
};
