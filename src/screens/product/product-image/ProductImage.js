import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './ProductImage.css';

export class ProductImage extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { loading: true };
  }

  static getDerivedStateFromProps({ image }, state) {
    if (state.image === image.big) {
      return null;
    } else {
      return { ...state, loading: true, image: image.big };
    }
  }

  onLoadHandler = () => {
    this.setState({ loading: false });
  }

  render() {
    const className = 'ProductImage' + (this.state.loading ? ' ProductImage--loading' : '');

    return (
      <div className={className}>
        <img alt="" src={ this.state.image } onLoad={ this.onLoadHandler } />
      </div>
    )
  }
};

ProductImage.propTypes = {
  image: PropTypes.shape({
    big: PropTypes.string.isRequired
  })
};
