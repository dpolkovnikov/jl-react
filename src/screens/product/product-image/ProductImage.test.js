import React from 'react';
import renderer from 'react-test-renderer';

import { ProductImage } from './ProductImage';

describe('ProductImage _snapshot_', () => {
  it('ProductImage renders correctly', () => {
    const productImage = renderer
      .create(<ProductImage image={{ big: 'imageSrc' }} />)
      .toJSON();

    expect(productImage).toMatchSnapshot();
  });
});
