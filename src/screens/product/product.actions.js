import { getPhotos } from './media';

export const CHANGE_IMAGE = 'CHANGE_IMAGE';
export const changeImage = newImage => ({
  type: CHANGE_IMAGE,
  payload: newImage
});


export const SET_IMAGES = 'SET_IMAGES';
export const setImages = images => ({
  type: SET_IMAGES,
  payload: images
});

export const SELECT_CATEGORY = 'SELECT_CATEGORY';
export const selectCategory = category => ({
  type: SET_IMAGES,
  payload: category
});

export const loadImages =
  ({ setIndex = 0, category } = {}) =>
    dispatch =>
      getPhotos(category).then(images => {
        dispatch(setImages(images));
        dispatch(changeImage(images[setIndex]));
      })
