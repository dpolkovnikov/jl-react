import { SET_IMAGES, CHANGE_IMAGE, SELECT_CATEGORY } from './product.actions';

const initialState = {
  image: { big: '' },
  images: [{ small: '', id: '' }]
};

export const ProductData = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_IMAGES:
      return {
        ...state,
        images: payload
      }
    case CHANGE_IMAGE:
      return {
        ...state,
        image: payload
      }
    case SELECT_CATEGORY:
      return {
        ...state,
        category: payload
      }
    default:
      return state;
  }
};
