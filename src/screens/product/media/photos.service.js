import { unsplash } from './photos.api';

const toJSON = async response => JSON.parse(await response.text());
const toImagesModel = photos => photos.map(({ id, urls: { regular: big, thumb: small } }) => ({ id, big, small }));

export const getPhotos = async category => {
  const response = await unsplash.search.photos(category, 1, 5)
  const { results:jsonModel } = await toJSON(response);
  const imagesModel = toImagesModel(jsonModel);

  return imagesModel;
}
