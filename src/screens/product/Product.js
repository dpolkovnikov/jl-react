import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './Product.css';
import { changeImage, loadImages, selectCategory } from './product.actions';

import { Thumbnails } from './thumbnails';
import { ProductImage } from './product-image';

export class Product extends Component {
  componentDidMount() {
    const { match: { params: { category } } } = this.props;

    // this.props.loadImages({ category });

    // this.state = {};
  }

  changeImageHandler = image => {
    this.props.changeImage(image);
  }

  render() {
    return (
      <section className="Product">
        <h2>The Photos!</h2>

        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link className="navbar-brand" to="/product/latest">Categories</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Features</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Pricing</a>
              </li>
              <li className="nav-item">
                <a className="nav-link disabled" href="#">Disabled</a>
              </li>
            </ul>
          </div>
        </nav>

        <ProductImage image={this.props.image} />

        <Thumbnails images={this.props.images} onClick={this.changeImageHandler} />
      </section>
    )
  }
}

const mapStateToProps = ({ ProductData: { image, images } }, { match: { params: { category: categoryName } } }) => {
  return { image, images };
};

const mapDispatchToProps = dispatch => ({
  changeImage: image => {
    dispatch(changeImage(image));
  },
  loadImages: options => {
    dispatch(loadImages(options));
  },
  selectCategory: category => {
    dispatch(selectCategory(category));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);

Product.propTypes = {
  image: PropTypes.object.isRequired,
  images: PropTypes.array.isRequired,
  changeImage: PropTypes.func.isRequired,
  loadImages: PropTypes.func.isRequired
}
