import React from 'react';
import PropTypes from 'prop-types';

import './Thumbnail.css';

export const Thumbnail = props => (
  <li className="Thumbnail">
    <button type="button">
      <img alt="" src={ props.imageSrc } />
    </button>
  </li>
);

Thumbnail.propTypes = {
  imageSrc: PropTypes.string.isRequired
};
