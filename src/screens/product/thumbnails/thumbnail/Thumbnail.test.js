import React from 'react';
import renderer from 'react-test-renderer';

import { Thumbnail } from './Thumbnail';

describe('Thumbnail _snapshot_', () => {
  it('Thumbnail renders correctly', () => {
    const thumbnail = renderer
      .create(<Thumbnail imageSrc='imageSrc' />)
      .toJSON();

    expect(thumbnail).toMatchSnapshot();
  });
});
