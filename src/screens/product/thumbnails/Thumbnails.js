import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './Thumbnails.css';

import { Thumbnail } from './thumbnail';

export class Thumbnails extends PureComponent {
  onClick = event => {
    const children = [ ...event.currentTarget.children ];

    event.preventDefault();

    const index = children.indexOf(children.find( child => (
      (child === event.target) || child.contains(event.target)
    )));
    const image = index === -1 ? false : this.props.images[index];

    image && this.props.onClick(image);
  }

  render() {
    return (
      <ul className="Thumbnails" onClick={ this.onClick }>
        {
          this.props.images.map( ({ small, id }) => (
            <Thumbnail key={ id }
                       imageSrc={ small } />
          ))
        }
      </ul>
    )
  }
}

Thumbnails.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape({
    small: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
  })),
  onClick: PropTypes.func.isRequired
};
