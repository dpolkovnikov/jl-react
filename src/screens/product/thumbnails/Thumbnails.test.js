import React from 'react';
import { shallow } from 'enzyme';

import { Thumbnails } from './Thumbnails';
import { Thumbnail } from './thumbnail';

jest.mock('./thumbnail', () => ({ Thumbnail: () => { } }))

describe('Thumbnails', () => {
  test('Thumbnails component renders empty list', () => {
    const thumbnails = shallow(<Thumbnails onClick={() => { }} images={[]} />);

    expect(thumbnails.text()).toEqual('');
  });

  test('Thumbnails component renders not empty list', () => {
    const thumbnails = shallow(<Thumbnails onClick={() => { }} images={[
      { small: 'imageSrc', id: '' },
      { small: 'imageSrc', id: '' }
    ]} />);

    expect(thumbnails.find(Thumbnail).length).toEqual(2);
  });

  test('Thumbnails component triggers onClick handler with image', () => {
    const mockClick = jest.fn();
    const image = { small: 'imageSrc', id: '' };

    const thumbnails = shallow(<Thumbnails onClick={mockClick} images={[image]} />);
    const thumbnail = thumbnails.find(Thumbnail).get(0);

    thumbnails.simulate('click', {
      currentTarget: { children: [thumbnail] },
      preventDefault: () => { },
      target: thumbnail
    });

    expect(mockClick).toHaveBeenCalledWith(image);
  });
});
