import React from 'react';

import { Header } from './header';

export const Layout = ({ children }) => (
  <div>
    <Header />
    <div className="container py-5">
      {children}
    </div>
  </div>
);
