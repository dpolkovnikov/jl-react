import React from 'react';
import renderer from 'react-test-renderer';

import { Header } from './Header';

describe('Header _snapshot_', () => {
  it('Header renders correctly', () => {
    const header = renderer
      .create(<Header />)
      .toJSON();

    expect(header).toMatchSnapshot();
  });
});
