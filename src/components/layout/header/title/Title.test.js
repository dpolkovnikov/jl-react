import React from 'react';
import renderer from 'react-test-renderer';

import { Title } from './Title';

describe('Title _snapshot_', () => {
  it('Title renders correctly', () => {
    const title = renderer
      .create(<Title />)
      .toJSON();

    expect(title).toMatchSnapshot();
  });
});
