import React from 'react';

import './Title.css';

export const Title = () => (
  <h1 className="Title">Welcome to React</h1>
);
