import React from 'react';
import renderer from 'react-test-renderer';

import { Logo } from './Logo';

describe('Logo _snapshot_', () => {
  it('Logo renders correctly', () => {
    const logo = renderer
      .create(<Logo />)
      .toJSON();

    expect(logo).toMatchSnapshot();
  });
});
