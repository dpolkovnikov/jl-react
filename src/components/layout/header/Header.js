import React from 'react';

import './Header.css';

import { Title } from './title';
import { Logo } from './logo';

export const Header = () => (
  <header className="Header">
    <Logo />
    <Title />
  </header>
);
