import { createStore } from 'redux';
import { rootReducer } from './root.reducer';
import { middleware } from './middleware';

export const store = createStore(rootReducer, middleware);
