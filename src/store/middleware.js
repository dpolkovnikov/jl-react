import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

let middleware = [ thunk ];

if (process.env.NODE_ENV === 'development') {
  const logger = require('redux-logger').createLogger();
  middleware = [ ...middleware, logger ];
}

middleware = applyMiddleware( ...middleware );

export { middleware };
