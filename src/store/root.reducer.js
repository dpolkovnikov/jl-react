import { combineReducers } from 'redux';
import { componentsReducers } from '../components';
import { screensReducers } from '../screens';

export const rootReducer = combineReducers({ ...componentsReducers, ...screensReducers });
